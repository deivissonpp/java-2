package com.bhlangonijr.service;

import static org.thymeleaf.util.StringUtils.isEmptyOrWhitespace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bhlangonijr.domain.Message;
import com.bhlangonijr.domain.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

    private static final Logger log = LoggerFactory.getLogger(MessageService.class);
    private static HashMap<String, Message> localDb = new HashMap<>();

    /**
     * Validate and store this message into the database
     *
     * @param message
     * @return
     * @throws Exception
     */
    public Response send(Message message) {
        Response response = new Response();
        if (messageIsInvalid(message) || messageAlreadyExists(message)) {
            response.setSuccess(false);
            response.setText("Message cannot be sourced by martians!");
            log.error(response.getText());
        } else {
            localDb.put(message.getId(), message);
            response.setSuccess(true);
            response.setText(message.getText().concat(" - has been processed"));
            log.info(response.getText());
        }

        return response;
    }

    private boolean messageAlreadyExists(Message message) {
        return getById(message.getId()) != null;
    }

    private boolean messageIsInvalid(Message message) {
        return message == null || isEmptyOrWhitespace(message.getId())
                || isEmptyOrWhitespace(message.getFrom())
                || isEmptyOrWhitespace(message.getTo())
                || isEmptyOrWhitespace(message.getText());
    }

    public List<Message> getAllMessages() {
        return new ArrayList<>(localDb.values());
    }

    public Message getById(String id) {
        return localDb.get(id);
    }

}
